#!/bin/bash

F9_3_FILE="./kicad-library-utils/klc-check/rules_footprint/F9_3.py"
sed -i 's|KICAD7_3DMODEL_DIR|AALTO_FABLAB|g' "${F9_3_FILE}"

FP_DIR="AaltoFablab.pretty"

for file in "${FP_DIR}"/*.kicad_mod; do
    if [ -f "${file}" ]; then
        ./kicad-library-utils/klc-check/check_footprint.py -w "${file}"
    fi
done
